﻿using AppConsultaViaCep.Views;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppConsultaViaCep
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new ListagemCepView());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
